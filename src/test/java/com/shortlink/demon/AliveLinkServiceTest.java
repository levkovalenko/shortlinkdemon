package com.shortlink.demon;

import com.shortlink.demon.entity.Link;
import com.shortlink.demon.service.AliveLinkService;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class AliveLinkServiceTest {
  private AliveLinkService service = new AliveLinkService(10L);

  @Test
  public void isALive() {
    String baseUrl = "http://spring.io";
    Link expected =
        new Link(1L, baseUrl, "c174bc557cb3b644f3658669f7bd6e65", OffsetDateTime.now());
    StepVerifier.create(service.isAlive(Mono.just(expected)))
        .expectNextCount(1)
        .expectComplete()
        .verify();

    expected.setCreatedAt(OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZoneOffset.ofHours(0)));
    StepVerifier.create(service.isAlive(Mono.just(expected)))
        .expectNextCount(0)
        .expectComplete()
        .verify();
  }
}
