package com.shortlink.demon;

import static org.assertj.core.api.Assertions.assertThat;

import com.shortlink.demon.entity.Link;
import com.shortlink.demon.service.ShortLinkService;
import org.junit.jupiter.api.Test;

public class ShortLinkServiceTest {
  private ShortLinkService service = new ShortLinkService();

  @Test
  public void shortensLink() {
    String baseUrl = "http://spring.io";
    String fakeUrl = "https://spring.io";
    Link expected = new Link(baseUrl, "j0vPxo5KM5");
    assertThat(service.shortlink(baseUrl).getHashKey()).isEqualTo(expected.getHashKey());
    assertThat(service.shortlink(fakeUrl).getHashKey()).isNotEqualTo(expected.getHashKey());
  }
}
