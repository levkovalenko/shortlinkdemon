package com.shortlink.demon.repository;

import com.shortlink.demon.entity.Link;
import java.time.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@Qualifier("redisRepository")
public class RedisRepository {
  @Autowired
  @Qualifier("redisUserTemplate")
  private final ReactiveRedisOperations<String, String> redisTemplate;

  private final Long aliveMinutes;

  /**
   * Repository for caching links.
   *
   * @param aliveMinutes cache alive minutes.
   * @param redisTemplate api for redis operations.
   */
  @Autowired
  public RedisRepository(
      @Value("${app.aliveMinutes}") Long aliveMinutes,
      @Qualifier("redisUserTemplate") ReactiveRedisOperations<String, String> redisTemplate) {
    this.aliveMinutes = aliveMinutes;
    this.redisTemplate = redisTemplate;
  }

  public Mono<Boolean> saveLink(Link link) {
    return redisTemplate
        .opsForValue()
        .set(link.getHashKey(), link.getLink())
        .then(redisTemplate.expire(link.getHashKey(), Duration.ofMinutes(aliveMinutes)));
  }

  public Mono<Link> getLink(String key) {
    return redisTemplate.opsForValue().get(key).mapNotNull(url -> new Link(url, key));
  }
}
