package com.shortlink.demon.repository;

import com.shortlink.demon.entity.Link;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

@Qualifier("linkRepository")
public interface LinkRepository extends ReactiveCrudRepository<Link, Long> {

  /**
   * save link into database
   *
   * @param link link for save.
   * @return saved link.
   */
  Mono<Link> save(Link link);

  /**
   * method for select last link by HashKey.
   *
   * @param key key for selecting.
   * @return last link selected by Hash key.
   */
  Mono<Link> findFirstByHashKeyOrderByCreatedAtDesc(String key);
}
