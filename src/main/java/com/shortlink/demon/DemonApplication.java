package com.shortlink.demon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@SpringBootApplication
@EntityScan("com.shortlink.demon")
@EnableR2dbcRepositories
public class DemonApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemonApplication.class, args);
  }
}
