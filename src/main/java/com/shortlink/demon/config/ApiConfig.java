package com.shortlink.demon.config;

import com.fasterxml.classmate.TypeResolver;
import java.util.Arrays;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@RequiredArgsConstructor
public class ApiConfig {

  private final TypeResolver resolver;
  /**
   * Configuration of swagger api.
   *
   * @return configurated Docket.
   */
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.shortlink.demon.controller"))
        .paths(PathSelectors.any())
        .build()
        .pathMapping("/")
        .apiInfo(apiInfo())
        .useDefaultResponseMessages(false)
        .alternateTypeRules(
            new RecursiveAlternateTypeRule(
                resolver,
                Arrays.asList(
                    AlternateTypeRules.newRule(
                        resolver.resolve(Mono.class, WildcardType.class),
                        resolver.resolve(WildcardType.class)),
                    AlternateTypeRules.newRule(
                        resolver.resolve(ResponseEntity.class, WildcardType.class),
                        resolver.resolve(WildcardType.class)))));
  }

  /**
   * APi description for swagger.
   *
   * @return api info.
   */
  @Bean
  public ApiInfo apiInfo() {
    final ApiInfoBuilder builder = new ApiInfoBuilder();
    builder
        .title("ShortLink API through Swagger UI")
        .version("1.0")
        .license("(C) Copyright Test")
        .description("List of all the APIs of ShortLinkApp through Swagger UI");
    return builder.build();
  }
}
