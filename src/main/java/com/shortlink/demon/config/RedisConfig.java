package com.shortlink.demon.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

  @Value("${spring.redis.host}")
  private String redisHost;

  @Value("${spring.redis.port}")
  private int redisPort;

  /**
   * create reactive connection factory for redis.
   *
   * @return reactive connection factory.
   */
  @Bean
  public ReactiveRedisConnectionFactory reactiveRedisConnectionFactory() {
    RedisStandaloneConfiguration redisStandaloneConfiguration =
        new RedisStandaloneConfiguration(redisHost, redisPort);
    return new LettuceConnectionFactory(redisStandaloneConfiguration);
  }

  /**
   * create reactive redis operations template.
   *
   * @return reactive redis operations.
   */
  @Bean
  @Qualifier("redisUserTemplate")
  ReactiveRedisOperations<String, String> redisLinkTemplate() {
    RedisSerializationContext.RedisSerializationContextBuilder<String, String> builder =
        RedisSerializationContext.newSerializationContext(new StringRedisSerializer());
    RedisSerializationContext<String, String> context =
        builder.value(new StringRedisSerializer()).build();
    ReactiveRedisConnectionFactory factory = reactiveRedisConnectionFactory();
    return new ReactiveRedisTemplate<>(factory, context);
  }
}
