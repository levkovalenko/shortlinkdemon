package com.shortlink.demon.config;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories
public class PostgresConfig extends AbstractR2dbcConfiguration {

  @Value("${spring.r2dbc.host}")
  private String databaseHost;

  @Value("${spring.r2dbc.port}")
  private int databasePort;

  @Value("${spring.r2dbc.name}")
  private String databaseName;

  @Value("${spring.r2dbc.username}")
  private String databaseUsername;

  @Value("${spring.r2dbc.password}")
  private String databasePassword;

  /** Create connection factory for postgres. */
  @Override
  @Bean
  public ConnectionFactory connectionFactory() {
    return new PostgresqlConnectionFactory(
        PostgresqlConnectionConfiguration.builder()
            .host(databaseHost)
            .port(databasePort)
            .database(databaseName)
            .username(databaseUsername)
            .password(databasePassword)
            .build());
  }
}
