package com.shortlink.demon.service;

import com.shortlink.demon.entity.Link;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class AliveLinkService {

  private final Long aliveMinutes;

  /**
   * AliveService constructor.
   *
   * @param aliveMinutes link alive minutes.
   */
  public AliveLinkService(@Value("${app.aliveMinutes}") Long aliveMinutes) {
    this.aliveMinutes = aliveMinutes;
  }

  /**
   * Check ttl of requested link.
   *
   * @param link link for checking.
   * @return Mono<Link> or Mono<Void>.
   */
  public Mono<Link> isAlive(Mono<Link> link) {
    OffsetDateTime current = OffsetDateTime.now();
    return link.filter(
        result -> result.getCreatedAt().until(current, ChronoUnit.MINUTES) < aliveMinutes);
  }
}
