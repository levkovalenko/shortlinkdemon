package com.shortlink.demon.service;

import com.shortlink.demon.controller.contracts.ShortLink;
import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;
import java.time.Duration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@Service
public class LimitResponseService {
  private final Bucket bucket;

  /**
   * Request limit period setup.
   *
   * @param shortLinkLimit limit of connections.
   * @param shortLinkPeriod connection limit period.
   */
  public LimitResponseService(
      @Value("${app.connection.limit}") int shortLinkLimit,
      @Value("${app.connection.period}") long shortLinkPeriod) {
    Bandwidth limit =
        Bandwidth.classic(
            shortLinkLimit, Refill.of(shortLinkLimit, Duration.ofSeconds(shortLinkPeriod)));
    this.bucket = Bucket4j.builder().addLimit(limit).build();
  }

  /**
   * Check is request limit exceeded.
   * TODO: add inner exception class, not http
   *
   * @return none if correct, error if it is exceeded
   */
  public Mono<ShortLink> checkLimit() {
    if (!bucket.tryConsume(1)) {
      return Mono.error(
          new ResponseStatusException(HttpStatus.TOO_MANY_REQUESTS, "Too many requests."));
    }
    return Mono.empty();
  }
}
