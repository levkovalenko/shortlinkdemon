package com.shortlink.demon.service;

import com.shortlink.demon.entity.Link;
import com.shortlink.demon.repository.LinkRepository;
import com.shortlink.demon.repository.RedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CacheService {

  private final String baseUrl;
  private final LinkRepository linkRepository;
  private final RedisRepository redisRepository;

  /**
   * Service for cahing links.
   *
   * @param baseUrl app base url.
   * @param linkRepository repository for db.
   * @param redisRepository repository for redis.
   */
  @Autowired
  public CacheService(
      @Value("${app.baseUrl}") String baseUrl,
      @Qualifier("linkRepository") LinkRepository linkRepository,
      @Qualifier("redisRepository") RedisRepository redisRepository) {
    this.baseUrl = baseUrl;
    this.linkRepository = linkRepository;
    this.redisRepository = redisRepository;
  }

  /**
   * Save link into cache and set expire.
   *
   * @param link link for saving.
   * @return saved link.
   */
  public Mono<String> saveLink(Link link) {
    return redisRepository
        .saveLink(link)
        .then(linkRepository.save(link).map(result -> baseUrl + result.getHashKey()));
  }

  /**
   * Get link from cache or db.
   *
   * @param key short link.
   * @return full link.
   */
  public Mono<Link> getLink(String key) {
    return redisRepository
        .getLink(key)
        .switchIfEmpty(linkRepository.findFirstByHashKeyOrderByCreatedAtDesc(key));
  }
}
