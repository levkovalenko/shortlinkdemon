package com.shortlink.demon.service;

import com.shortlink.demon.entity.Link;
import org.hashids.Hashids;
import org.springframework.stereotype.Service;

@Service
public class ShortLinkService {

final private Hashids hashids = new Hashids("", 8);

  /**
   * Get hash of link and reverse link
   *
   * @param link link for encoding
   * @return long hash
   */
  private Long getHash(String link) {
    long upper = ((long) link.hashCode()) << 16;
    long lower = ((long) link.hashCode());
    return Math.abs(upper + lower) % Hashids.MAX_NUMBER;
  }
  /**
   * Get 8 symbols hash code for link.
   *
   * @param link full url.
   * @return short link.
   */
  public Link shortlink(String link) {
    String hashKey = hashids.encode(getHash(link));
    return new Link(link, hashKey);
  }
}
