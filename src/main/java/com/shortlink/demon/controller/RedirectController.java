package com.shortlink.demon.controller;

import com.shortlink.demon.entity.Link;
import com.shortlink.demon.service.AliveLinkService;
import com.shortlink.demon.service.CacheService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class RedirectController {
  private final CacheService cacheService;
  private final AliveLinkService aliveLinkService;

  /**
   * Redirect by short link.
   *
   * @param key short link key.
   * @return redicrect response.
   */
  @ApiOperation(value = "Redirect by short link")
  @ApiResponses(
      value = {
        @ApiResponse(code = 308, message = "Redirection over short link."),
        @ApiResponse(code = 404, message = "Link not found."),
      })
  @ResponseStatus(HttpStatus.PERMANENT_REDIRECT)
  @GetMapping("/{key}")
  Mono<ResponseEntity<Object>> redirect(@PathVariable String key) {
    Mono<Link> link = cacheService.getLink(key);
    return aliveLinkService
        .isAlive(link)
        .map(
            result ->
                ResponseEntity.status(HttpStatus.PERMANENT_REDIRECT)
                    .header("Location", result.getLink())
                    .build())
        .switchIfEmpty(
            Mono.error(
                new ResponseStatusException(HttpStatus.NOT_FOUND, "no original link found")));
  }
}
