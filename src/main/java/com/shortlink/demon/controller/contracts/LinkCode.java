package com.shortlink.demon.controller.contracts;

import lombok.Value;

@Value
public class LinkCode {
    private String linkCode;
}
