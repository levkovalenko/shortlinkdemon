package com.shortlink.demon.controller.contracts;

import lombok.Value;

@Value
public class OriginalLink {
  private String link;
}
