package com.shortlink.demon.controller.contracts;

import lombok.Value;

@Value
public class ShortLink {
  private String shortLink;
}
