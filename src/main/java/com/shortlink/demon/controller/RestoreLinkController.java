package com.shortlink.demon.controller;

import com.shortlink.demon.controller.contracts.LinkCode;
import com.shortlink.demon.controller.contracts.OriginalLink;
import com.shortlink.demon.service.CacheService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class RestoreLinkController {

  private final CacheService cacheService;

  /**
   * Endpoint for restoring original links by hash code.
   *
   * @param request request with link hash.
   * @return orginal link.
   */
  @ApiOperation(value = "Get original link by hash key.", code = 200)
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Original Link."),
        @ApiResponse(code = 404, message = "No original link found."),
      })
  @ResponseStatus(HttpStatus.OK)
  @PostMapping("/link")
  @CrossOrigin(origins = "*", methods = RequestMethod.POST)
  Mono<OriginalLink> restore(@RequestBody LinkCode request) {
    return cacheService
        .getLink(request.getLinkCode())
        .mapNotNull(result -> result.getLink())
        .mapNotNull(OriginalLink::new)
        .switchIfEmpty(
            Mono.error(
                new ResponseStatusException(HttpStatus.NOT_FOUND, "no original link found")));
  }
}
