package com.shortlink.demon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class SwaggerController {
  /**
   * Swagger ui endpoint.
   *
   * @return redirect to swagger.
   */
  @RequestMapping(method = RequestMethod.GET)
  public String swaggerUi() {
    return "redirect:/swagger-ui/index.html";
  }
}
