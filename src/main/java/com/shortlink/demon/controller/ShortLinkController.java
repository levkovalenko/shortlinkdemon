package com.shortlink.demon.controller;

import com.shortlink.demon.controller.contracts.OriginalLink;
import com.shortlink.demon.controller.contracts.ShortLink;
import com.shortlink.demon.entity.Link;
import com.shortlink.demon.service.CacheService;
import com.shortlink.demon.service.LimitResponseService;
import com.shortlink.demon.service.ShortLinkService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class ShortLinkController {

  private final ShortLinkService shortLinkService;
  private final CacheService cacheService;
  private final LimitResponseService limitResponseService;

  /**
   * Endpoint for creating short links.
   *
   * @param request request with link.
   * @return shorted link.
   * @throws ResponseStatusException max limit requests error
   */
  @ApiOperation(value = "Get short link.", code = 200)
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Short Link."),
        @ApiResponse(code = 429, message = "Max number of connections per moment"),
      })
  @ResponseStatus(HttpStatus.OK)
  @PostMapping("/shortlink")
  @CrossOrigin(origins = "*", methods = RequestMethod.POST)
  Mono<ShortLink> create(@RequestBody OriginalLink request) throws ResponseStatusException {
    Link shortlink = shortLinkService.shortlink(request.getLink());
    return limitResponseService
        .checkLimit()
        .switchIfEmpty(cacheService.saveLink(shortlink).map(link -> new ShortLink(link)));
  }
}
