package com.shortlink.demon.entity;

import java.time.OffsetDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table("link")
public class Link {

  @Id private Long id;

  private String link;
  private String hashKey;
  private OffsetDateTime createdAt;

  /**
   * Constructor for Link.
   *
   * @param link full url.
   * @param hashKey md5 hash of the link.
   */
  public Link(String link, String hashKey) {
    this.link = link;
    this.hashKey = hashKey;
    this.createdAt = OffsetDateTime.now();
  }
}
