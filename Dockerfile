FROM openjdk:11

RUN apt-get update && apt-get install -y tini

COPY build/libs/*SNAPSHOT.jar ./app.jar

ENTRYPOINT [ "tini", "-g", "--" ]

EXPOSE 8080

CMD [ "sh", "-c", "java -jar app.jar" ]
